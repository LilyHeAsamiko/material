# FYS-4096 Computational Physics, exercise 3

Return your solution to project 'exercise3' under your GitLab group for this
course by Friday 5 AM. 

Tag the final version with `final` keyword and make sure to include a file
`problems_solved` in the repository. The `problems_solved`-file should be a
comma separated list of problems you have solved.

# Problems

These exercises have you visualize data from many different sources. You can
download all the data from [https://www.tut.fi/fys/fys4096/ex3data.tar](https://www.tut.fi/fys/fys4096/ex3data.tar).

The solution to each problem should be a python script with name like `problem1.py` that
  
  1. saves a figure / movie file in 'media'-directory under your gitlab root and 
  2. exits after it has saved the file.

**Do not include the contents of the 'media'-directory in your Git repo!**

Your assignment is to **visualize each dataset in an appropriate matter and
save the visualization in 'media'-directory**. Your solutions will be graded
based on 

* Your choise of visualization technique
* How awesome your visualization looks
* Savefiles (format, size, ...)
* How easy your python scripts are to understand. 
* PEP8-compliance

As this exercise has you create not a python package but just some scripts, no
unit-testing or packaging is required. A 'README.rst'-file and a license file should 
still be included together with docstrings for the scripts.

## 1. 2 years of temperature measurements in downtown Tampere (1 XP)

File `ex3_tampere_downtown_weather.npz`. This file is a multi-object,
compressed binary format used by `numpy`. You can load the file into Python
with

```python
data = numpy.load(filename)`
```

Datasets inside the file can be queried with ```data.keys()``` and accessed like
this:

```python
data['temperatures']
```

**Visualize the 2-year temperature measurement data.**

## 2. Predicted temperatures in Scandinavia (single time instant) (1 XP)

File `ex3_temperatures_Jan20th2018_18:00_onwards_54_hours.h5`. This file is a
multi-dataset HDF5 binary file. It can be loaded with many modern programming
languages and multitude of software including, e.g, Matlab and Mathematica.

You can load this file and access the contents like:
```python
file = h5py.File('myfilename.h5', 'r')
list(file.keys())
list(file['temperatures'].attrs)
attribute = file['temperatures'].attrs['Description']
temperatures = file['temperatures'][:]
```

**In this problem, pick a single time instant and visualize the predicted temperatures.**

## 3. Predicted temperatures in Scandinavia (54 hours) (1 XP)

Same as problem 2, but now you should somehow **visualize the entire 54 hour
prediction of temperatures in Scandinavia.**

## 4. Population structure of Finland in the year 1900 (1 XP)

File `ex3_Finland_population_structure_1865_2016.h5`.

**Visualize the population structure of Finland in 1900.**

## 5. Population structure of Finland from 1866 to 2016 (1 XP)

File `ex3_Finland_population_structure_1865_2016.h5`.

**Visualize the population structure of Finland from 1866 to 2016.**

## 6. Static electric field in the vicinity of a gold nanodevice (2 XP)

File `ex3_bowtie_electric_field.h5`.

**Visualize the electric field.**

## 7. Energy-levels of graphene (1 XP)

File `ex3_graphene_bands.h5`.

**Visualize conductance and valence bands of graphene in the same figure.**

## 8. Figures in publications (1 XP)

In the downloaded package, you will also find an example LaTeX template for a
physics paper to be published in _Physical Review Letters_. Write a script
that saves an image named 'media/paper_figure.ZZZ' where 'ZZZ' corresponds to
your chosen file format. Use the data from problem 1 to create the plot.

You will be graded based on 
  1. how good the figure looks
  2. how well the figure integrates to the PDF generated by

```bash
$ pdflatex paper.tex
```

## 9. Advanced licensing (1 XP)

So far you've defaulted to Boost Software License for your codes. When creating
images or multimedia, it's a good practice to license them also. However, none
of our typical software licenses (BSL, GSL, MIT, ...) is well suited for
licensing other media.

**In this exercise, you will pick two licenses. Copy the license texts to
'license_codes.txt' and 'license_other_media.txt'.**

The first license should be for your scripts/codes. The latter for the
figures/videos they create.

**Write to README.rst the reasoning for choosing these particular licenses.**

You can search for short summaries of licenses at [tldrlegal](http://www.tldrlegal.com). 
Popular licenses for software include, e.g., BSL, GPL, LGPL, MIT, and BSD.

For figures, media, and other content, some popular ~open licenses include,
e.g., [Creative Commons licenses](https://creativecommons.org/choose/?lang=en).

## 10. Extra, feedback (1 XP)

Provide feedback and suggestions on the course so far via [Webropol](https://www.webropolsurveys.com/S/D29BC414D93B47D9.par). Please note
that Webropol is notoriously slow :(

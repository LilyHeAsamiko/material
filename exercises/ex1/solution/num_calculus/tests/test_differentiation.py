#!/usr/bin/env python3

import unittest

import num_calculus.differentiation as diff_calc


class TestFirstDerivative(unittest.TestCase):
    def test_constant(self):
        """ Differentiation tests with a constant function """
        def const_fun(x):
            return 1.0

        self.assertAlmostEqual(diff_calc.eval_derivative(const_fun, 2.0, 1e-6),
                               0.0)

        self.assertAlmostEqual(diff_calc.eval_derivative(const_fun, 1.0, 1e-3),
                               0.0)

    def test_linear(self):
        """ Differentiation tests with a linear affine function """
        def lin_fun(x):
            return 0.5 + 2 * x

        self.assertAlmostEqual(diff_calc.eval_derivative(lin_fun, 2.0, 1e-6),
                               2)

        self.assertAlmostEqual(diff_calc.eval_derivative(lin_fun, -2.0, 1e-6),
                               2)

    def test_quadratic(self):
        """ Differentiation tests with a 2nd order polynomial """
        def quad_fun(x):
            return 0.1 * x**2 + x - 3.2

        self.assertAlmostEqual(diff_calc.eval_derivative(quad_fun, 2.0, 1e-6),
                               1.4, places=4)

        self.assertAlmostEqual(diff_calc.eval_derivative(quad_fun, -2.0, 1e-6),
                               0.6, places=4)

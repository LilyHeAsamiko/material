#!/usr/bin/env python3
"""
Implementation of differentiation routines.
"""

import numpy as np
from .type_checking import *

def eval_derivative(function, x, dx):
    """
    Evaluates the derivative of the given
    function with forward difference (1st order).

    Parameters
    ----------
    function    callable
                This should be a single-argument function returning a float
    x           float or np.ndarray of floats
                The value/values where to evaluate the derivative
    dx          float / np.ndarray of floats
                The difference step
    Returns
    -------
    deriv       float or np.ndarray of floats
                The value of the derivative of the function
    """

    assert callable(function)
    assert (type(x) is float) or is_numpy_array_of_reals(x)
    assert (type(dx) is float) or is_numpy_array_of_reals(dx)

    assert not (type(x) is np.ndarray and type(dx) is np.ndarray)

    deriv = (function(x + dx) - function(x)) / dx

    assert (type(deriv) is float) or is_numpy_array_of_reals(deriv) 

    return deriv

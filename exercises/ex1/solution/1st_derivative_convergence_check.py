#!/usr/bin/env python3

"""
A script for plotting the error scaling of our implemented
finite-difference differentiation scheme.
"""

# Import packages that we need
import numpy as np
import matplotlib.pyplot as plt

from num_calculus.differentiation import eval_derivative

# This is my <3 test function


def fun(x):
    return np.tanh(-x)


# Create an array of values
dx = np.linspace(1e-5, 0.1, 10000)

x = 2.0


derivatives = np.array(eval_derivative(fun, x, dx))
errors = np.abs(-1 / np.cosh(x)**2 - derivatives)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(dx, errors)

ax.set_xlabel(r'$\mathrm{d}x$')
ax.set_ylabel(r'Absolute error')

plt.savefig('figure.pdf')
plt.show()

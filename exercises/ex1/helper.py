#!/usr/bin/env python3

import pycodestyle
import glob
import unittest

from pathlib import Path


def check_docstring(self, obj):
    self.assertTrue(hasattr(obj, '__doc__'),
                    msg=repr(obj) + " has no package level docstring")

    self.assertNotEqual(obj.__doc__, None,
                        msg=repr(obj) + " has zero length docstring")

    self.assertTrue(len(obj.__doc__) != 0,
                    msg=repr(obj) + " has zero length docstring")


def dir_exists(path):
    """Checks if the given path exists and is a file."""
    return Path(path).is_dir()


def file_exists(path):
    """Checks if the given path exists and is a file."""
    return Path(path).is_file()


def empty(path):
    """Checks if the given directory path contains python source files."""
    if dir_exists(path):
        py_files = glob.glob(path + "/*.py")
        return len(py_files) > 0
    else:
        return False


class TestDirectoryStructure(unittest.TestCase):
    longMessage = False

    def test1_has_init(self):
        self.assertTrue(dir_exists("num_calculus"),
                        msg="""No directory 'num_calculus'
                               for this exercise's module.
                               Have you used another name
                               or is it missing?""")
        self.assertTrue(file_exists("num_calculus/__init__.py"),
                        msg="""No file 'num_calculus/__init__.py'
                               It's needed
                               to have Python realize
                               this directory contains a package.""")

    def test2_tests_at_default_locations(self):
        self.assertTrue(dir_exists("num_calculus/tests"),
                        msg="No 'tests' submodule.")
        self.assertTrue(file_exists(
            "num_calculus/tests/__init__.py"), msg="No __init__.py in tests.")

    def test3_non_code_files(self):
        self.assertTrue(file_exists("requirements.txt"),
                        msg="No requirements.txt")
        self.assertTrue(file_exists("license.txt"), msg="No license.txt")
        self.assertTrue(file_exists("README.rst"), msg="No README.rst")
        self.assertTrue(file_exists(".gitignore"), msg="No .gitignore")


class TestModuleFunctionality(unittest.TestCase):

    def test1_import(self):
        import num_calculus

    def test2_import_differentiation(self):
        import num_calculus.differentiation

    def test3_function_exists(self):
        from num_calculus.differentiation import eval_derivative

    def test4_function_works(self):
        from num_calculus.differentiation import eval_derivative

        def const_fun(x):
            return 1.0

        self.assertAlmostEqual(eval_derivative(const_fun, 2.0, 1e-6),
                               0.0)

        self.assertAlmostEqual(eval_derivative(const_fun, 1.0, 1e-3),
                               0.0)

        def lin_fun(x):
            return 0.5 + 2 * x

        self.assertAlmostEqual(eval_derivative(lin_fun, 2.0, 1e-6),
                               2)

        self.assertAlmostEqual(eval_derivative(lin_fun, -2.0, 1e-6),
                               2)

        def quad_fun(x):
            return 0.1 * x**2 + x - 3.2

        self.assertAlmostEqual(eval_derivative(quad_fun, 2.0, 1e-6),
                               1.4, places=3)

        self.assertAlmostEqual(eval_derivative(quad_fun, -2.0, 1e-6),
                               0.6, places=3)

    def test5_check_docstring(self):

        import num_calculus.differentiation

        check_docstring(self, num_calculus.differentiation.eval_derivative)


class TestPEP8Compliance(unittest.TestCase):

    def test_pep8(self):
        import pycodestyle
        import glob

        style = pycodestyle.StyleGuide(quiet=True)

        py_sources = glob.glob('**/*.py', recursive=True)

        report = style.check_files(py_sources)

        self.assertEqual(report.total_errors, 0,
                         msg="""Your code is not PEP-8 compliant:
                            it contains stylistic mistakes.
                            Run 'pycodestyle -r .' to
                            get a list of mistakes.""")


class TestDocstrings(unittest.TestCase):

    def test_package_lvl_docstrings(self):
        import num_calculus as nc

        self.assertTrue(hasattr(nc, '__doc__'),
                        msg="num_calculus has no package level docstring")

        self.assertNotEqual(nc.__doc__, None,
                            msg="num_calculus.tests has zero length docstring")

        self.assertTrue(len(nc.__doc__) != 0,
                        msg="num_calculus has zero length docstring")

        self.assertTrue(hasattr(nc, '__author__'),
                        msg="num_calculus module has no author information")

        self.assertTrue(hasattr(nc, '__copyright__'),
                        msg="num_calculus_module has no copyright information")

        self.assertTrue(hasattr(nc, '__author_email__'),
                        msg="""num_calculus module has no author
                             email information""")

        self.assertTrue(hasattr(nc, '__license__'),
                        msg="num_calculus module has no license information")

        self.assertTrue(hasattr(nc, '__version__'),
                        msg="num_calculus module has no version information")

    def test_test_docstring(self):
        import num_calculus as nc
        import num_calculus.tests

        self.assertTrue(hasattr(nc.tests, '__doc__'),
                        msg="num_calculus.tests has no docstring")

        self.assertNotEqual(nc.tests.__doc__, None,
                            msg="num_calculus.tests has zero length docstring")

    def test_file_docstring(self):
        import num_calculus as nc
        import num_calculus.differentiation

        check_docstring(self, nc.differentiation)


if __name__ == '__main__':
    unittest.main()

FYS-4096: Exercise 2: Numerical calculus package
================================================

This package contains an example solution for exercise 2 of FYS-4096
Computational Physics.

The numerical work is in 'scripts' -directory whereas the implemented
methods can be found under 'num_calculus'.

.. image:: ../data.png

#!/usr/bin/env python3

from setuptools import setup, find_packages

import num_calculus as my_pkg

setup(name='num_calculus',
      author=my_pkg.__author__,
      author_email=my_pkg.__author_email__,
      classifiers=[
          'License :: OSI Approved :: Boost Software License 1.0',
          'Programming Language :: Python :: 3.6',
          'Topic :: Numerical Mathematics :: Calculus',
      ],
      description='Numerical calculus routines.',
      install_requires=[],
      keywords='numerics calculus differentiation',
      license=my_pkg.__license__,
      packages=find_packages(),
      test_suite='nose.collector',
      tests_require=['nose'],
      url='https://compphys.solanpaa.fi',
      version=my_pkg.__version__,
      zip_safe=True)

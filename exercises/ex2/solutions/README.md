## Derivation of a custom 2nd order finite-difference differentiation scheme for 1st derivatives

We have a function $`f : \mathbb{R} \to \mathbb{R}`$ whose values we can 
evaluate at $`x -\frac{1}{3}h,\, x,\, x+\frac{1}{4}h,\, x+\frac{1}{3}h`$. Our 
goal is to derive a second order approximation for $`f'(x)`$ using the function 
values at those points.

We begin by writing the Taylor expansion of $`f(x)`$ up to and including third 
order in $`h`$ at different points:

$`f\left( x - \frac{1}{3}h\right) = f(x) - f'(x) \frac{1}{3}h
                                    + f''(x) \frac{1}{18}h^2 
                                    - f'''(x) \frac{1}{162}h^3 
                                    + \mathcal{O}(h^4)`$

$`f\left( x + \frac{1}{4}h\right) = f(x) + f'(x) \frac{1}{4}h
                                    + f''(x) \frac{1}{32}h^2 
                                    + f'''(x) \frac{1}{384}h^3 
                                    + \mathcal{O}(h^4)`$

$`f\left( x + \frac{1}{3}h\right) = f(x) + f'(x) \frac{1}{3}h
                                    + f''(x) \frac{1}{18}h^2 
                                    + f'''(x) \frac{1}{162}h^3
                                    + \mathcal{O}(h^4)`$

Next, we multiply these on both sides by $`a,\,b`$, and $`c`$, respectively, 
and add them up to obtain

$`a f\left( x - \frac{1}{3}h\right) + b f\left( x + \frac{1}{4}h\right) 
    + c f\left( x + \frac{1}{3}h\right) 
  = (a+b+c) f(x) + ( -\frac{1}{3}a + \frac{1}{4}b + \frac{1}{3}c )h f'(x) 
    + (\frac{1}{18}a + \frac{1}{32}b + \frac{1}{18}c) f''(x) h^2 
    + (-\frac{1}{162}a +\frac{1}{384}b +\frac{1}{162}c)f'''(x)h^3
    + \mathcal{O}(h^4)`$

Solving for $`f'(x)`$ gives

$` f'(x) = \frac{12}{h} \left[ \frac{a+b+c}{4a-3b-4c}f(x) 
           - \frac{a}{4a-3b-4c} f(x-\frac{1}{3}h) 
           - \frac{b}{4a-3b-4c} f(x+\frac{1}{4}h)
           - \frac{c}{4a-3b-4c} f(x+\frac{1}{3}h) \right] 
           + \frac{16a+9b+16c}{24(4a-3b-4c)}f''(x)h 
           + \frac{-64a+27b+64c}{3456 a - 2592 b - 3456 c}f'''(x) h^2 
           + \mathcal{O}(h^3)  `$

For this to be a **2nd order** scheme, we require the first and second order 
error terms to be zero, i.e.,

$`16a+9b+16c = 0`$ and

$`-64a +27b+64c = 0`$.

This equation pair has the solution $`a=\frac{c}{7}`$ 
and $`b = -\frac{128c}{63}`$. Plugging these into our formula for $`f'(x)`$ 
and simplifying yields

$`f'(x) = \frac{-9 f\left( x - \frac{1}{3}h\right) - 56 f(x) 
                  + 128 f\left( x + \frac{1}{4}h\right) 
                  - 63 f\left( x + \frac{1}{3}h\right) }
                {14h} + \mathcal{O}(h^3),`$

which is, indeed, a second order finite-difference scheme.

#!/usr/bin/env python3

"""
This script shows examples of numerical integration techniques for problems in
exercise 2D
"""

from scipy.special import j1, jn_zeros, gamma
from scipy.integrate import simps, quad
import numpy as np


if __name__ == '__main__':
    # \int_0^\infty J_1(x) dx

    def short_j1_integral(a, b):
        """ Calculates integral of j1 over a short interval [a,b] with
        Simpson's rule using 100 sampling points including the endpoints of the
        interval"""
        x = np.linspace(a, b, 100)
        return simps(j1(x), x)

    def calculate_j1_integral(num_intervals):
        """ Calculates integral of j1 from o 0 to 'num_intervals' root of j1,
        e.g., up to 5th root of j1 """
        intervals = np.zeros(num_intervals + 1)
        intervals[1:] = jn_zeros(1, num_intervals)

        integrals = [short_j1_integral(intervals[i], intervals[i + 1])
                     for i in range(num_intervals)]
        res = np.sum(integrals)
        return res

    # Integrate j1 over a longer and longer section until we get sufficient
    # convergence

    # First we initialize the algorithm
    abserr = 1e-2  # max. absolute error of the integral
    result_previous = 0
    n_zeros = 10
    result_next = calculate_j1_integral(n_zeros)
    # Next we start iterating until we get convergence
    while np.fabs(result_next - result_previous) > abserr:
        n_zeros *= 2
        result_previous = result_next
        result_next = calculate_j1_integral(n_zeros)

    print("\int_0^1 J_1(x) dx ≈ %.4f" % result_next)

    # \int\limits_2^{15} \mathrm{Re}\left\{ \log[ \mathrm{\Gamma}(-x)] \right\}
    # This function has singularities at integer x (observe by, e.g., plotting
    # it or from some earlier course). We do a trick where we calculate
    # the integral in a complex plane, just above the real axis and at the end
    # just take the real part as it should be almost the same as the integral
    # over the real axis. This trick is valid only for sufficiently
    # well-behaved integrands.

    eps = 1e-6  # How much we step outside of the real-axis
    x = np.linspace(2, 15, 10000)  # Sampling points on real axis
    z = x + eps * 1j  # True sampling points

    res = simps(np.log(np.abs(gamma(-z))), x)

    print("\int\limits_2^{15}  \log[ | G(-x) | ] ≈ %.4f" % res)

    # \int_{2-radius circle on complex plane}  1/(z-1) dz
    # Use a parametric representation for the path: γ(s) = 2cos(s) + 2sin(s) i
    s = np.linspace(0, 2 * np.pi, 1000)
    path = 2 * np.cos(s) + 2j * np.sin(s)

    # Calculate midpoint of each path segment and evaluate integrand there
    z_mid = (path[1:] + path[:-1]) / 2.0
    integrand = 1 / (z_mid - 1)

    # Calculate the differential path segment
    dz = np.diff(path, axis=-1)

    # Mid-point Riemann sum as our numerical integration algorithm
    res = np.sum(integrand * dz)

    print("""\int_{2-radius circle on complex plane}  1/(z-1) dz\
 ≈ %.4f + %.4f i""" % (res.real, res.imag))

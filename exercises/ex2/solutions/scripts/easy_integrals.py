#!/usr/bin/env python3

"""
Numerical evaluation of a few easy integrals with trapezoidal
and simpson's rules
"""

from scipy.integrate import *
import numpy as np


def calculate_and_print_integral(integrand, integral_as_str):
    """
    Calculates the integral of 'integrand' from 0 to 5 with trapezoidal and
    Simpson's rule and compares them to a numerically exact (adaptive
    quadrature) value.

    Parameters
    ----------

    integrand           callable
                        the integrand
    integral_as_str     string
                        textual representation of the integral to be performed

    Returns
    -------
    None
    """
    assert callable(integrand)

    x = np.arange(0, 5.05, 0.1)
    y = integrand(x)

    res_trapez = trapz(y, x)
    res_simps = simps(y, x)
    res_exact, _ = quad(integrand, 0, 5, epsrel=1e-6)
    print(integral_as_str)
    print("Trapezoidal rule: %.4e" % res_trapez)
    print("Simpson's rule  : %.4e" % res_simps)
    print("Exact           : %.4e" % res_exact)
    print("")


if __name__ == '__main__':
    calculate_and_print_integral(lambda x: x**2, " \int_0^5 x^2 dx")

    calculate_and_print_integral(lambda x: np.exp(
        np.sin(x**3)), " \int_0^5 exp(sin(x^3)) dx")

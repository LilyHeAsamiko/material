#!/usr/bin/env python3

"""
Some example ways to evaluate the integrals in 2C numerically.
"""

import numpy as np
from scipy.integrate import simps

if __name__ == '__main__':
    # \int_0^1 x^(-1/2) dx
    # Summary: avoid sampling the integrand at x=0
    #
    # 1. Divide the interval to subintervals of equal length
    # 2. Evaluate the function at the midpoint of each interval
    # 3. Sum the values and multiply by the subinterval length to get the value
    # of the integral
    #
    # See the book Numerical Recipes in C++: The Art of Scientific Computing,
    # page 167
    x = np.linspace(0, 1, 10000)
    dx = x[1] - x[0]
    x_mid = (x[:-1] + x[1:]) / 2.0
    y_mid = 1.0 / np.sqrt(x_mid)

    res = dx * np.sum(y_mid)
    print("\int_0^1 x^(-1/2) dx ≈ %.4f" % res)

    ###########################################################################
    ###########################################################################

    # \int_0^1 sin(x)/x dx
    # Summary: avoid sampling the integrand at x=0
    #
    # 1. Divide the interval to subintervals of equal length
    # 2. Evaluate the function at the midpoint of each interval
    # 3. Sum the values and multiply by the subinterval length to get the value
    # of the integral
    #
    # See the book Numerical Recipes in C++: The Art of Scientific Computing,
    # page 167
    x = np.linspace(0, 1, 10000)
    dx = x[1] - x[0]
    x_mid = (x[:-1] + x[1:]) / 2.0
    y_mid = np.sin(x_mid) / x_mid

    res = dx * np.sum(y_mid)
    print("\int_0^1 x^(-1/2) dx ≈ %.4f" % res)

    ###########################################################################
    ###########################################################################

    # \int_0^\infty exp(-x) dx
    # Change of variables u = arctan(x)
    #   => \int_0^{pi/2} exp(-tan(u))/cos(u)^2 du

    u = np.linspace(0, np.pi / 2.0, 1000)
    du = u[1] - u[0]
    y = np.exp(-np.tan(u)) / np.cos(u)**2
    res = simps(y, u)
    print("\int_0^\infty exp(-x) dx ≈ %.4f" % res)

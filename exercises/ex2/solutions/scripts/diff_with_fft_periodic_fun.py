#!/usr/bin/env python3

"""
This script demonstrates the use of FFT for differentiating
a periodic function.
"""

import numpy as np
import matplotlib.pyplot as plt
from num_calculus.differentiation import deriv_fft, eval_derivative_fd

if __name__ == '__main__':
    # Function to be differentiated
    def fun(x): return 2 * np.sin(36 / 7. * np.pi * x) * np.sin(np.pi / 7 * x)

    # Sample function values
    x = np.linspace(0, 7, 1000)
    y = fun(x)

    # Evaluate the derivatives
    y_diff = eval_derivative_fd(x, y)
    y_diff_fft = deriv_fft(x, y)

    # Plot results
    plt.plot(x, y_diff)
    plt.plot(x, y_diff_fft)
    plt.xlabel(r'$x$')
    plt.ylabel(r"$y'(x)$")
    plt.show()

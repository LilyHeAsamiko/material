#!/usr/bin/env python3

"""
A script for evaluating the derivatives for the Mysterious Stranger.
This script also plots the datapoints and shows the linear approximation
obtained with the derivative to visually confirm its validity.
"""

import sys
sys.path.append("../")

import numpy as np
from num_calculus.differentiation import eval_derivative_mysterious_stranger
import matplotlib.pyplot as plt

if __name__ == '__main__':
    # Datapoints for the derivative at t=0.2 hours
    t = np.array([0.1333333333, 0.2, 0.25, 0.26666666666666])
    f = np.array([0.399936791, 0.399680042, 0.399219004, 0.398989068])

    der0_2 = eval_derivative_mysterious_stranger(t, f)

    # Plot the data and a linear approximation obtained with the derivative
    plt.scatter(t, f)
    tc = np.linspace(0, 1, 1000)
    f = der0_2 * (tc - t[1]) + f[1]
    plt.plot(tc, f)

    # Datapoints for the  derivative at t=0.6 hours
    t = np.array([0.53333333333333, 0.6, 0.65, 0.66666666666666])
    f = np.array([0.383927081, 0.374358729, 0.364826674, 0.361139867])

    plt.scatter(t, f)
    der0_6 = eval_derivative_mysterious_stranger(t, f)

    # Plot the data and a corresponding linear approximation
    tc = np.linspace(0, 1, 1000)
    f = der0_6 * (tc - t[1]) + f[1]
    plt.plot(tc, f)

    # Output the values of the derivative
    print(" f'(t = 0.2 hours) ≈ %.4f" % der0_2)
    print(" f'(t = 0.6 hours) ≈ %.4f" % der0_6)

    # Name axes etc.
    plt.xlabel(r'time (hours)')
    plt.ylabel(r'$f(t)$')

    plt.show()

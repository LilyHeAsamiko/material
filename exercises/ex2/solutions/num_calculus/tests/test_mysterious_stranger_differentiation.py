#!/usr/bin/env python3

import unittest

import num_calculus.differentiation as diff_calc
import numpy as np


class TEST_FD_diff_mysterious(unittest.TestCase):
    """ Tests for the mysterious stranger differentiation problem """

    def test_nonmysterious_input(self):
        """ Test when inputting data with incorrect spacing """
        x = np.linspace(0, 1, 4)
        with self.assertRaises(AssertionError):
            diff_calc.eval_derivative_mysterious_stranger(x, x)

    def test_uneq_length_inputs(self):
        """ Test when inputting arrays of different length """
        x = np.linspace(0, 1, 4)
        with self.assertRaises(AssertionError):
            diff_calc.eval_derivative_mysterious_stranger(x, x[:-1])

    def test_too_long_inputs(self):
        """ Test when argument array too long """
        x = np.linspace(0, 1, 5)
        with self.assertRaises(AssertionError):
            diff_calc.eval_derivative_mysterious_stranger(x, x)

    def test_too_short_inputs(self):
        """ Test with too short input arrays """
        x = np.linspace(0, 1, 3)
        with self.assertRaises(AssertionError):
            diff_calc.eval_derivative_mysterious_stranger(x, x)

    def test_list_input(self):
        """ Test with inputs as list instead of numpy arrays """
        x = [1.0, 2.0, 3.0, 4.0]
        with self.assertRaises(TypeError):
            diff_calc.eval_derivative_mysterious_stranger(x, x)

    def test_int_input(self):
        """ Test with inputs as numpy arrays of integers """
        x = np.array([0, 4, 7, 8], dtype=int)
        diff_calc.eval_derivative_mysterious_stranger(x, x)

    def test_derivative_values_exp(self):
        """ Test correctness of the derivative with exp(x) """
        h = 1e-5
        x0 = 3

        def fun(x): return np.exp(x)
        x = np.array([x0 - h / 3, x0, x0 + h / 4, x0 + h / 3])
        y = fun(x)

        self.assertAlmostEqual(
            diff_calc.eval_derivative_mysterious_stranger(x, y),
            np.exp(3)
        )

    def test_derivative_values(self):
        """ Test correctness of the derivative with sinh(x)"""
        h = 1e-5
        x0 = 3

        def fun(x): return np.sinh(x)
        x = np.array([x0 - h / 3, x0, x0 + h / 4, x0 + h / 3])
        y = fun(x)

        self.assertAlmostEqual(
            diff_calc.eval_derivative_mysterious_stranger(x, y),
            np.cosh(3)
        )

#!/usr/bin/env python3

import unittest

import num_calculus.differentiation as diff_calc
import numpy as np
import numpy.testing as npt


class TEST_FFT_diff(unittest.TestCase):
    """ Tests for a differentiation scheme using FFT """

    def test_uneq_length_inputs(self):
        """ Test when inputting arrays of different length """
        x = np.linspace(0, 1, 40)
        with self.assertRaises(AssertionError):
            diff_calc.deriv_fft(x, x[:-1])

    def test_list_input(self):
        """ Test with inputs as list instead of numpy arrays """
        x = [1.0, 2.0, 3.0, 4.0]
        with self.assertRaises(TypeError):
            diff_calc.deriv_fft(x, x)

    def test_int_input(self):
        """ Test with inputs as numpy arrays of integers """
        x = np.array([1, 2, 3, 4], dtype=int)
        diff_calc.deriv_fft(x, x)

    def test_derivative_values_cos(self):
        """ Test correctness of the derivative with exp(x) """

        def fun(x): return np.cos(2 * np.pi * x)
        x = np.linspace(0, 1, 100000)[:-1]
        y = fun(x)

        npt.assert_allclose(
            diff_calc.deriv_fft(x, y),
            -2 * np.pi * np.sin(2 * np.pi * x),
            atol=1e-5
        )

    def test_derivative_values_sinh_sin(self):
        """ Test correctness of the derivative with exp(x) """

        def fun(x): return np.sinh(np.sin(2 * np.pi * x))
        x = np.linspace(0, 1, 100000)[:-1]
        y = fun(x)

        npt.assert_allclose(
            diff_calc.deriv_fft(x, y),
            2 * np.pi * np.cos(2 * np.pi * x) * np.cosh(np.sin(2 * np.pi * x)),
            atol=1e-5
        )

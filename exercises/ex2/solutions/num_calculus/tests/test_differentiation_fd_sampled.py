#!/usr/bin/env python3

import unittest

import num_calculus.differentiation as diff_calc
import numpy as np
import numpy.testing as npt


class TEST_FD_diff(unittest.TestCase):
    """ Tests for first order differentiation scheme """

    def test_uneq_length_inputs(self):
        """ Test when inputting arrays of different length """
        x = np.linspace(0, 1, 4)
        with self.assertRaises(AssertionError):
            diff_calc.eval_derivative_fd(x, x[:-1])

    def test_list_input(self):
        """ Test with inputs as list instead of numpy arrays """
        x = [1.0, 2.0, 3.0, 4.0]
        with self.assertRaises(TypeError):
            diff_calc.eval_derivative_fd(x, x)

    def test_int_input(self):
        """ Test with inputs as numpy arrays of integers """
        x = np.array([1, 2, 3, 4], dtype=int)
        diff_calc.eval_derivative_fd(x, x)  # should pass

    def test_derivative_values_exp(self):
        """ Test correctness of the derivative with exp(x) """
        h = 1e-5

        def fun(x): return np.exp(x)
        x = np.linspace(0, 1, 10000)
        y = fun(x)

        npt.assert_allclose(
            diff_calc.eval_derivative_fd(x, y),
            np.exp(x),
            rtol=1e-4
        )

    def test_derivative_values_sinh(self):
        """ Test correctness of the derivative with sinh(x) """
        h = 1e-5

        def fun(x): return np.sinh(x)
        x = np.linspace(0, 1, 10000)
        y = fun(x)

        npt.assert_allclose(
            diff_calc.eval_derivative_fd(x, y),
            np.cosh(x),
            rtol=1e-4
        )

#!/usr/bin/env python3
"""
Implementation of differentiation routines.
"""

import numpy as np
from .type_checking import *


def eval_derivative_fd(x, y):
    """
    Evaluates the derivative of the sampled function with forward difference
    (1st order).

    Parameters
    ----------
    x           np.ndarray of floats
                The argument values where the function is sampled
    y           np.ndarray of floats
                The function values at the sampling points
    Returns
    -------
    deriv       float or np.ndarray of floats
                The value of the derivative of the function
    """
    if not is_numpy_array_of_reals(x):
        raise TypeError("x is not a numpy array of real numbers")
    if not is_numpy_array_of_reals(y):
        raise TypeError("y is not a numpy array of real numbers")
    assert len(x) == len(y)

    deriv = np.empty(len(x))
    # Forward-difference
    deriv[:-1] = np.diff(y) / np.diff(x)
    # Last derivative with backward-difference
    deriv[-1] = deriv[-2]

    return deriv


def eval_derivative_mysterious_stranger(x, y):
    """
    Evaluates the derivative of the sampled function where samples are given at
    x-h/3, x, x+h/4, and x+h/3. The derivative at x is returned.

    Parameters
    ----------
    x           np.ndarray of floating point numbers, shape = (4,)
                The argument values where the function is sampled. Should have
                spacing as specified above and be in increasing order.
    y           np.ndarray of floating point numbers, shape = (4,)
                The function values at the sampling points
    Returns
    -------
    deriv       float
                The value of the derivative of the function at x[1]
    """

    if not is_numpy_array_of_reals(x):
        raise TypeError("x is not a numpy array of reals but %s" % repr(x))
    if not is_numpy_array_of_reals(y):
        raise TypeError("x is not a numpy array of reals but %s" % repr(y))

    assert len(x) == 4, "Inputs should be of length 4"
    assert len(y) == 4, "Inputs should be of length 4"
    h0 = (x[1] - x[0]) * 3
    h1 = (x[2] - x[1]) * 4
    h2 = (x[3] - x[1]) * 3

    assert np.isclose(h0, h1) and np.isclose(h1, h2), \
        "Spacing of x-values is inconsistent with this method: %f, %f, %f" % (
            h0, h1, h2)

    return (-9 * y[0] - 56 * y[1] + 128 * y[2] - 63 * y[3]) / (14 * h0)


def deriv_fft(x, y):
    """
    Calculates the derivative of a periodic y(x) using FFT on equidistantly
    sampled values.

    Parameters
    ----------
    x       np.ndarray of floating point numbers, shape = (N,)
            Argument values, should be equidistantly spaced
    y       np.ndarray of floating point numbers, shape = (N,)
            Function values at points given by 'x'

    Returns
    -------
    deriv   np.ndarray of floating point numbers, shape = (N,)

    """
    if not is_numpy_array_of_reals(x):
        raise TypeError(
          "x is not a numpy array of real numbers: type(x) = %s" % repr(type(x)))
    if not is_numpy_array_of_reals(y):
        raise TypeError(
          "y is not a numpy array of real numbers: type(y) = %s" % repr(type(y)))
    assert len(x) == len(y), "Arguments should be of equal length"
    assert np.fabs(x[1] - x[0]) > 0, \
        "Step-size should be positive"
    assert np.allclose(np.diff(x), x[1] - x[0]), \
        "Step-size should be constant"

    N = len(x)

    F = np.fft.fft(y)

    w = 2 * np.pi * np.fft.fftfreq(n=N, d=x[1] - x[0])

    if N % 2 == 0:
        w[N // 2] = 0

    return np.fft.ifft(1j * w * F).real

"""
This is an example solution for exercise 2 of the course
FYS-4096 Computational Physics at Tampere University of
Technology, Spring 2018.

The package contains methods for evaluation of numerical
derivative of single-argument, real-valued functions.
"""

__author__ = "Janne Solanpää"
__copyright__ = "Copyright 2017-2018, Janne Solanpää"
__author_email__ = "janne+compphys@solanpaa.fi"
__license__ = "Boost Software License 1.0"
__version__ = "0.1"

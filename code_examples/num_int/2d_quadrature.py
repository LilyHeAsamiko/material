#!/usr/bin/env python3
import numpy as np
from scipy.integrate import simps

# Integrand sampling points in each dimension
x = np.linspace(0, 5, 200)
y = np.linspace(1, 6, 50)

# Sampling points
X, Y = np.meshgrid(x, y)

# Integrand


def fun(x, y): return x**2 * np.exp(-x * y / np.exp(x))


fun_values = fun(X, Y)

# Integrate over x
int_dx = simps(fun_values, dx=x[1] - x[0], axis=0)

# Integrate over y
result = simps(int_dx, dx=y[1] - y[0])

print(result)

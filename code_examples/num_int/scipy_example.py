#!/usr/bin/env python3
import numpy as np
import scipy.integrate

x = np.linspace(0, 5, 1000)


def fun(x): return x**2 + 3 * np.cos(x * np.exp(x))


result = scipy.integrate.trapz(y=fun(x), x=x)
result = scipy.integrate.simps(y=fun(x), x=x)

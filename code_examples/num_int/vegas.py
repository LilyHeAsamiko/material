#!/usr/bin/env python3

import numpy as np
import vegas


def fun(x): return 1 / (1 + np.exp(-np.sum(x**2)))


integrator = vegas.Integrator([[0, 3], [5, 6], [-5, 7]])

# Warm-up
integrator(fun, nitn=5, neval=1000)

# Integrate
result = integrator(fun, nitn=10, neval=1000)

print(result)

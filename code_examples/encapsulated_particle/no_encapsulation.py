#!/usr/bin/env python3

import numpy as np

# particle states as [[ x1, y1, vx1, vy1, m1 ], [x2, y2, vx2, vy2, m2],...]
particles = np.array([[0.0, 1.0, 1.0, 1.0, 3.0], [
                     1.2, 3.4, -1.1, 4.3, 4.0], [4.3, 2.3, -1.2, 2.3, 5.0]])
N = particles.shape[0]
# Print out all particle positions
for i in range(N):
    print("(x,y) = (%.2f,%.2f), (vx,vy) = (%.2f,%.2f), m = %.1f" %
          tuple(particles[i, :]))


def grav_force(x0, y0, m0, x1, y1, m1):
    return m0 * m1 / np.sqrt((x0 - x1)**2 + (y0 - y1)**2)**3 * np.array([x1 - x0, y1 - y0])


# Calculate acceleration of all the particles
# a = F/m
acceleration = np.zeros((N, 2))
for i in np.arange(N):
    for j in np.arange(N):
        if i != j:
            acceleration[i, :] += grav_force(particles[i, 0], particles[i, 1], particles[i, 4],
                                             particles[j, 0], particles[j, 1], particles[j, 4]) / particles[i, 4]
        else:
            pass

# Print out acceleration
for i in range(N):
    print("(ax,ay) = (%.2f,%.2f)" % tuple(acceleration[i, :]))

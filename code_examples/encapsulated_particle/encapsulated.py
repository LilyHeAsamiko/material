#!/usr/bin/env python3

import numpy as np


class Particle(object):
    aliases = {
        'r': 'position',
        'v': 'velocity',
        'a': 'acceleration',
        'm': 'mass',
    }

    def __init__(self, position, velocity, mass):
        self.position = np.array(position)
        self.velocity = np.array(velocity)
        self.acceleration = np.array([0.0, 0.0])
        self.mass = mass

    # https://stackoverflow.com/a/4017638
    def __setattr__(self, name, value):
        name = self.aliases.get(name, name)
        object.__setattr__(self, name, value)

    def __getattr__(self, name):
        if name == "aliases":
            raise AttributeError
        name = self.aliases.get(name, name)
        return object.__getattribute__(self, name)

def distance(p0, p1):
    return np.sqrt(sum((p0.r - p1.r)**2))

def grav_force(p0, p1):
    return (p1.r - p0.r) * p0.m * p1.m / distance(p0, p1)**3

# Main program starts here
particles = [
    Particle(position=(0.0, 1.0), velocity=(1.0, 1.0), mass=3.0),
    Particle(position=(1.2, 3.4), velocity=(-1.1, 4.3), mass=4.0),
    Particle(position=(4.3, 2.3), velocity=(-1.2, 2.3), mass=5.0),
]

for p in particles:
    print("(x,y) = (%.2f, %.2f), (vx,vy) = (%.2f,%.2f), m = %.1f"
          % (p.r[0], p.r[1], p.v[0], p.v[1], p.m))


# Calculate acceleration of all particles
for p1 in particles:
    for p2 in particles:
        if p1 != p2:
            p1.a += grav_force(p1, p2) / p1.m

for particle in particles:
    print("(ax,ay) = (%.2f,%.2f)" % (particle.a[0], particle.a[1]))

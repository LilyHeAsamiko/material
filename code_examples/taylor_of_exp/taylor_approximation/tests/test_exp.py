#!/usr/bin/env python3

"""
Tests for Taylor approximation of exp(x).
"""

import unittest
from taylor_approximation.exp import exp_approx


class test_exp_taylor(unittest.TestCase):
    def test_trivial(self):
        self.assertAlmostEqual(exp_approx(1.4, 0), 1.0)
        self.assertAlmostEqual(exp_approx(-5.5, 0), 1.0)

    def test_values(self):
        self.assertAlmostEqual(exp_approx(1.4, 5), 189479.0 / 46875)

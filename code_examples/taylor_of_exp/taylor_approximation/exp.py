#!/usr/bin/env python3

"""
Implementation of the Taylor expansion for exp(x) of real arguments
"""

import math


def exp_approx(x, order):
    """
    Calculates the nth order Taylor expansion of exp(x)
    expanded around x=0.

    Parameters
    ----------
    x           float
                The function is evaluated here.
    order       positive integer
                The order of the Taylor expansion

    Returns
    -------
    res         float
                The Taylor expansion.
    """

    # Check for inputs types
    assert type(x) is float
    assert type(order) is int
    assert order >= 0

    # Calculate the Taylor expansion
    res = 1.0
    for n in range(1, order + 1):
        res += 1 / math.factorial(n) * x**n

    # Check that our return value is sane
    assert type(res) is float
    assert not math.isnan(res)
    assert not math.isinf(res)

    return res

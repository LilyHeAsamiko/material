"""
This is an example package for the course FYS-4096 Computational Physics
at Tampere University of Technology.

The package contains methods for evaluation Taylor approximations of various
functions.
"""

__author__ = "Janne Solanpää"
__copyright__ = "Copyright 2017-2018, Janne Solanpää"
__author_email__ = "janne+compphys@solanpaa.fi"
__license__ = "Boost Software License 1.0"
__version__ = "0.1"

import taylor_approximation.exp

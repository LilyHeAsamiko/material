$ mkdir solve_high_T_superconductivity && cd solve_high_T_superconductivity

$ git init .

$ vim .gitignore 
...

$ git add .gitignore

$ git commit -m "Initial commit"

$ vim sc_model.py
...

$ git add sc_model.py

$ git commit -m "Added script for modeling superconductivity"

$ git remote add origin git@gitlab.com:solanpaa/high_temp_superconductivity.git

$ git push -u origin --all

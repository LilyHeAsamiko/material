#!/usr/bin/env python3

import h5py
import numpy as np

numbers = np.random.uniform(0.0, 1.0, 20)

avg = np.mean(numbers)

# Write data to file
with h5py.File(name='my_datafile.h5',
               mode='w') as datafile:
    datafile["/original_sample"] = numbers

    datafile["/original_sample"].attrs["description"] = """These are the original samples obtained from my highly-sensitive
        numerical experiment. The data is organized as follows..."""

    datafile["/statistics/mean"] = avg

# Read data from file
with h5py.File(name='my_datafile.h5',
               mode='r') as datafile:
    experimental_samples = datafile["/original_sample"][:]

    sample_avg = datafile["/statistics/mean"][()]

print("Experimental data was:\n" + str(experimental_samples))
print("Average value: %.2f" % sample_avg)

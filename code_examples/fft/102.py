#!/usr/bin/env python3

import numpy as np


def fun(x): return np.sin(x)**2


# Sampling points
x = np.linspace(0, 2 * np.pi, 300)[:-1]

# Samples
f = fun(x)

# Fourier coefficients (read about their order in NumPy docs)
F = np.fft.fft(f)

# Inverse fourier transform
f = np.fft.ifft(F)

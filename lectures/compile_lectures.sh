#!/bin/bash

# https://stackoverflow.com/a/246128
SOURCEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

slides=`ls -v ${SOURCEDIR}/slides/ |sort -h`

rm ${SOURCEDIR}/index.html
for slidefile in ${slides}
do
cat ${SOURCEDIR}/slides/$slidefile >> ${SOURCEDIR}/index.html
done

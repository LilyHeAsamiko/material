#!/usr/bin/env python3

import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

import numpy as np
import matplotlib.pyplot as plt

# Set figure shape to Golden Ratio
def set_gr(ax):
    x = ax.get_xlim()
    y = ax.get_ylim()
    gr = (np.sqrt(5.0)+1)/2
    ax.set_aspect((x[1]-x[0])/(y[1]-y[0])/gr)

gr = (np.sqrt(5)+1)/2
width = 0.013*246*2
height = width/gr
fig = plt.figure(figsize=(width, height))

ax = fig.add_subplot(111)

dx = 0.001
L = 4
x = np.arange(0, L, dx)

f = lambda x: np.sin(2*np.pi*x/4)*np.exp(-(x-2)**2)

ax.plot(x,f(x))

x_s = np.arange(0, L, 0.5)
N = len(x_s)


F = np.fft.fft(f(x_s))
k_s = np.arange(0, N, 1)

f_stupid_interp = lambda xr: [ 1/N* np.sum( F*np.exp(1j*2*np.pi * x/L * k_s)) for x in xr]

ax.plot(x, f_stupid_interp(x))
ax.scatter(x_s, f(x_s))
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$f$')

set_gr(ax)
fig.subplots_adjust(bottom=0.16)
plt.savefig("fft_stupid_interpolation.svg")


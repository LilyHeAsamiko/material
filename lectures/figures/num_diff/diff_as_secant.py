#!/usr/bin/env python3

import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

import numpy as np
import matplotlib.pyplot as plt

# Set figure shape to Golden Ratio
def set_gr(ax):
    x = ax.get_xlim()
    y = ax.get_ylim()
    gr = (np.sqrt(5.0)+1)/2
    ax.set_aspect((x[1]-x[0])/(y[1]-y[0])/gr)

gr = (np.sqrt(5)+1)/2
width = 0.013*246*2
height = width/gr
fig = plt.figure(figsize=(width, height))

ax = fig.add_subplot(111)

x = np.linspace(0,1)

def fun(x):
    return x + 0.3*np.sin(2*np.pi*x)

y = fun(x)

ax.plot(x,y)

x0 = 0.3
x1 = x0+0.15
y0 = fun(x0)
y1 = fun(x1)
k = (y1-y0)/(x1-x0)

ax.plot(x, k*(x-x0)+y0)
ax.scatter(x0,y0,marker='o',c='cyan', zorder=9)
ax.scatter(x1,y1,marker='o',c='cyan', zorder=9)

ax.axvline(x=x0, ymin=0, ymax=(y0-0.2)/0.6, linestyle='dashed')
ax.axvline(x=x1, ymin=0, ymax=(y1-0.2)/0.6, linestyle='dashed')

ax.axhline(y=y0, xmin=0, xmax=x0, linestyle='dashed')
ax.axhline(y=y1, xmin=0, xmax=x1, linestyle='dashed')

ax.set_xticks([x0, x1])
ax.set_xticklabels([r'$x_0$', r'$x_1$'])

ax.set_yticks([y0,y1])
ax.set_yticklabels([r'$y_0$', r'$y_1$'])
ax.set_ylim(0.2,0.8)
set_gr(ax)

plt.savefig('secant_example.svg')
plt.show()

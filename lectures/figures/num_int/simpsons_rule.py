#!/usr/bin/env python3

import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

import numpy as np
from scipy.interpolate import lagrange
import matplotlib.pyplot as plt

# Set figure shape to Golden Ratio
def set_gr(ax):
    x = ax.get_xlim()
    y = ax.get_ylim()
    gr = (np.sqrt(5.0)+1)/2
    ax.set_aspect((x[1]-x[0])/(y[1]-y[0])/gr)

gr = (np.sqrt(5)+1)/2
width = 0.013*246*2
height = width/gr
fig = plt.figure(figsize=(width, height))

ax = fig.add_subplot(111)

x = np.linspace(0,1)

def fun(x):
    return x + 0.3*np.sin(2*np.pi*x) -0.2

y = fun(x)

ax.plot(x,y)

ax.axhline(y=0)

# Sampling points/grid
x_s = np.linspace(0,1,5)

ax.scatter(x_s, fun(x_s))

# Plot the parabolas
f1 = lagrange(x_s[0:3],fun(x_s[0:3]))
x1 = np.linspace(x_s[0],x_s[2])

ax.fill_between(x1, 0, f1(x1), facecolor='cyan', alpha=0.5)
ax.plot(x1,f1(x1), c='cyan', ls='dashed')

f2 = lagrange(x_s[2:],fun(x_s[2:]))
x2 = np.linspace(x_s[2],x_s[-1])

ax.plot(x2,f2(x2), c='cyan', ls='dashed')
ax.fill_between(x2, 0, f2(x2), facecolor='cyan', alpha=0.5)

ax.plot([x_s[0], x_s[0]], [0, fun(x_s[0])], c='cyan', ls = 'dashed')

ax.plot([x_s[2], x_s[2]], [0, fun(x_s[2])], c='cyan', ls = 'dashed')

ax.plot([x_s[4], x_s[4]], [0, fun(x_s[4])], c='cyan', ls = 'dashed')

ax.set_xticks(x_s)

ax.set_xticklabels([r'$x_0$', r'$x_0+h$'] + [r'$x_0+%d h$'%(d+2) for d in range(len(x_s)-2)])

ax.set_yticks([])

set_gr(ax)

plt.savefig('simpsons_rule.svg')
plt.show()

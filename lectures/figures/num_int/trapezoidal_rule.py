#!/usr/bin/env python3

import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

import numpy as np
import matplotlib.pyplot as plt

# Set figure shape to Golden Ratio
def set_gr(ax):
    x = ax.get_xlim()
    y = ax.get_ylim()
    gr = (np.sqrt(5.0)+1)/2
    ax.set_aspect((x[1]-x[0])/(y[1]-y[0])/gr)

gr = (np.sqrt(5)+1)/2
width = 0.013*246*2
height = width/gr
fig = plt.figure(figsize=(width, height))

ax = fig.add_subplot(111)

x = np.linspace(0,1)

def fun(x):
    return x + 0.3*np.sin(2*np.pi*x) -0.2

y = fun(x)

ax.plot(x,y)

ax.axhline(y=0)

# Sampling points/grid
x_s = np.linspace(0,1,5)

ax.scatter(x_s, fun(x_s))

# Plot the trapezoids

ax.fill_between(x_s, 0, fun(x_s), facecolor='cyan', alpha=0.5)

for x in x_s:
    ax.plot([x,x],[0,fun(x)], color='cyan', ls='dashed')

ax.plot(x_s, fun(x_s), color='cyan', ls='dashed')

ax.set_xticks(x_s)
ax.set_xticklabels([r'$x_0$', r'$x_0+h$'] + [r'$x_0+%d h$'%(d+2) for d in range(len(x_s)-2)])

ax.set_yticks([])

set_gr(ax)

plt.savefig('trapezoidal_rule.svg')
plt.show()

#!/usr/bin/env python3

import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

import numpy as np
import matplotlib.pyplot as plt

def set_gr(ax):
    x = ax.get_xlim()
    y = ax.get_ylim()
    gr = (np.sqrt(5.0) + 1) / 2
    ax.set_aspect((x[1] - x[0]) / (y[1] - y[0]) / gr)


gr = (np.sqrt(5) + 1) / 2
width = 0.013 * 246 * 2
height = width / gr


fig = plt.figure(figsize=(width, height))
fig.subplots_adjust(bottom=0.18)
ax = fig.add_subplot(111)


def get_next_newton_point(fun, fder, x0):
    return x0-fun(x0)/fder(x0)
    

def plot_step(n, ax):
    def fun(x): return (x-1.3)**3-8
    def fder(x): return 3*(x-1.3)**2

    x = np.linspace(2.9, 3.5)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$y$')
    ax.plot(x, fun(x), c='w')
    ax.axhline(y=0, c='w')

    x0 = 3
    for i in range(n-1 if n > 0 else 0):
        x0 = get_next_newton_point(fun, fder, x0)
    prev_tang = None
    if n != 0:
        k = fder(x0)
        prev_tang, = plt.plot(x, k*(x-x0)+fun(x0), c='orange', ls='dashed')

        x0 = get_next_newton_point(fun, fder, x0)

    ax.axvline(x=x0, c='orange', linestyle='dashed')
    ax.set_ylim(-5,2.5)
    plt.savefig("newton_%0.2d.svg" % n)
    if prev_tang:
        ax.lines.remove(prev_tang)
    k = fder(x0)
    plt.plot(x, k*(x-x0)+fun(x0), c='orange', ls='dashed')
    ax.set_ylim(-5,2.5)
    plt.savefig("newton_%0.2d_2.svg" % n)
    plt.cla()


for i in range(5):
    plot_step(i, ax)

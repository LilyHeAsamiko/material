#!/usr/bin/env python3

import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

import numpy as np
import matplotlib.pyplot as plt

# Set figure shape to Golden Ratio


def set_gr(ax):
    x = ax.get_xlim()
    y = ax.get_ylim()
    gr = (np.sqrt(5.0) + 1) / 2
    ax.set_aspect((x[1] - x[0]) / (y[1] - y[0]) / gr)


gr = (np.sqrt(5) + 1) / 2
width = 0.013 * 246 * 2
height = width / gr


fig = plt.figure(figsize=(width, height))
fig.subplots_adjust(bottom=0.18)
ax = fig.add_subplot(111)


def get_next_bisect_bracket(fun, a, b):
    fa = fun(a)
    m = 0.5 * (a + b)
    fm = fun(m)
    fb = fun(b)
    if fa * fm < 0:
        return [a, m]
    elif fm * fb < 0:
        return [m, b]
    else:
        raise RuntimeError("Programming error?")


def plot_step(n, ax):
    def fun(x): return np.sin(x) * x - 0.4

    x = np.linspace(0, 1)
    ax.set_xlabel(r'$x$')
    ax.set_ylabel(r'$y$')
    ax.plot(x, fun(x), c='w')
    ax.axhline(y=0, c='w')

    ab = [0, 1]
    for i in range(n):
        ab = get_next_bisect_bracket(fun, ab[0], ab[1])

    ax.axvline(x=ab[0], c='orange')
    ax.axvline(x=ab[1], c='orange')

    plt.savefig("bisection_%0.2d.svg" % n)

    m = (ab[0] + ab[1]) * 0.5

    ax.axvline(x=m, c='orange', linestyle='dashed')
    plt.savefig("bisection_%0.2d_2.svg" % n)
    plt.cla()


for i in range(5):
    plot_step(i, ax)

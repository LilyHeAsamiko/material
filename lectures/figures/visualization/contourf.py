#!/usr/bin/env python3

# Use the style for lecture slides
import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')
import colorcet as cc

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# For creating a nice colorbar (from bogatron: https://stackoverflow.com/a/18195921)
from mpl_toolkits.axes_grid1 import make_axes_locatable

fig = plt.figure(figsize=(2*246/73.,1.2*246/73.))
ax = fig.add_subplot(111)

# Get example data for plotting
X, Y = np.mgrid[-5:5:200j,-5:5:200j]
im = ax.contourf(X,Y,X**2-Y*np.cos(X),5, cmap='magma')

# Create colorbar
divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cbar = plt.colorbar(im, cax=cax)
cbar.set_label(r'Fake data')

# Set axes labels
ax.set_xlabel(r'x')
ax.set_ylabel(r'y')

ax.set_aspect(1)
# Adjust the location of the axes and labels inside the canvas ('fig') so that
# everything is visible
fig.tight_layout(pad=1)

# Save as SVG for showing in browser
plt.savefig("contourf.svg", dpi=300)
plt.show()

#!/usr/bin/env python3

# Use the style for lecture slides
import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

# Some nice colormaps
import colorcet as cc

# For creating a nice colorbar (from bogatron: https://stackoverflow.com/a/18195921)
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(2*246/73.,1.7*246/73.))
ax = fig.add_subplot(111)

# Get example data for plotting
X, Y = np.mgrid[-10:10:15j,-10:10:15j]

F_x = Y
F_y = -X

ax.quiver(X, Y, F_x, F_y, color='white', pivot='mid')
# Set axes labels
ax.set_xlabel(r'$x$')
ax.set_ylabel(r'$y$')

ax.set_aspect(1)
# Adjust the location of the axes and labels inside the canvas ('fig') so that
# everything is visible
fig.tight_layout(pad=1)

# Save as SVG for showing in browser
plt.savefig("quiver.svg")
plt.show()

#!/usr/bin/env python3

# Use the style for lecture slides
import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

import numpy as np
import matplotlib.pyplot as plt

def axes_real_aspect_ratio_to_golden(ax):
    """ Sets axis size ratio to golden ratio """
    assert ax.get_xscale() == 'linear', "Only linear axes scales supported"
    assert ax.get_yscale() == 'linear', "Only linear axes scales supported"
    x = ax.get_xlim()
    y = ax.get_ylim()
    gr = (np.sqrt(5.0) + 1) / 2
    ax.set_aspect((x[1] - x[0]) / (y[1] - y[0]) / gr)

def get_figure(width = 246, aspect=(np.sqrt(5.0)+1)/2.0):
    """
    Returns a matplotlib figure. Default values produces figure whose
    width is 246 pt, i.e, the PRL column width and aspect ~ golden ratio.
    
    Parameters
    ----------
    width       int/float, optional
                width of the figure in 'pt' units (1 pt ≈ 0.3528 mm)
                Defaults to 246 pt, i.e., the width of a single column in
                Physical Review Letters
    aspect      int/float
                Sets width/height to this value

    Returns
    -------
    fig         matplotlib.pyplot.Figure
    """
    assert isinstance(width, (int, float, np.floating)), \
            "type(width) is not int,float or np.floating [was %s]"%repr(type(width))
    assert isinstance(aspect, (int, float, np.floating)), \
            "type(aspect) is not int,float or np.floating [was %s]"%repr(type(aspect))

    width = width/72. # in inches
    height = width / aspect

    return plt.figure(figsize=(width, height))

# Create a figure "canvas" whose width is 246 pt ≈ 8.7 cm
fig = get_figure(width = 492)
# Add axes on the figure ("canvas")
ax = fig.add_subplot(111)

# Create example data for plotting
x_joonas = np.linspace(0, 10, 10)
y_joonas = x_joonas+np.sinh(x_joonas)*np.exp(-(x_joonas/10)**2)/1000
yerr_plus_joonas = 3*np.sin(np.pi/10.*x_joonas)
yerr_minus_joonas = np.fabs(3*np.cos(np.pi/10.*x_joonas)+0.1)
x_janne = np.linspace(0.5, 10, 10)
y_janne = 10*(x_janne/10)**2+np.sinh(x_janne)*np.exp(-(x_janne/10)**2)/1000
yerr_janne = 3*np.sin(np.pi/10.*x_janne)
xerr_janne = 0.1*np.fabs(3*np.cos(np.pi/10.*x_janne)+0.1)

# Plot both datasets on the axes
ax.errorbar(x_joonas, y_joonas,
            yerr=np.vstack((yerr_minus_joonas,yerr_plus_joonas)),
            color='white', marker='o', 
            lw=0, elinewidth=1,
            capsize=3, capthick=1,
            label='Joonas')
ax.errorbar(x_janne, y_janne,
            xerr = xerr_janne,
            yerr = yerr_janne,
            color='orange', marker='^', 
            lw=0, elinewidth=1, 
            capsize=3,
            capthick=1,
            label='Janne')

# Set axes labels
ax.set_xlabel('time (minutes)')
ax.set_ylabel('distance to university (km)')

# Draw legend on the axes
ax.legend(loc='upper left')

# Set axes width/height to golden ratio 
axes_real_aspect_ratio_to_golden(ax)

# Adjust the location of the axes and labels inside the canvas ('fig') so that
# everything is visible
fig.tight_layout(pad=1)

# Save as SVG for showing in browser
plt.savefig("scatter_error.svg")

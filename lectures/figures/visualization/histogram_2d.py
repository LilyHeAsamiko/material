#!/usr/bin/env python3

# Use the style for lecture slides
import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

# Some nice colormaps
import colorcet as cc

# For creating a nice colorbar (from bogatron: https://stackoverflow.com/a/18195921)
from mpl_toolkits.axes_grid1 import make_axes_locatable

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

fig = plt.figure(figsize=(2*246/73.,1.7*246/73.))
ax = fig.add_subplot(111)

# Get example data for plotting
df = pd.read_csv('Finland_addresses_2017-05-15.csv')

lat = df['latitude_wgs84'].values
lon = df['longitude_wgs84'].values
# Sample in Pirkanmaa

hist2d, _, _ = np.histogram2d(lon, lat, bins=500, range=[[23.4,24],
                                                          [61.2,61.8]])

im = ax.imshow(hist2d.T*61.2, cmap=cc.cm['linear_kryw_5_100_c64'], origin='lower',
          interpolation='gaussian', extent=[23.4,24,61.2,61.8], vmin=0, vmax=1000, aspect='auto')

divider = make_axes_locatable(ax)
cax = divider.append_axes("right", size="5%", pad=0.05)
cbar = plt.colorbar(im, cax=cax)
cbar.set_label(r'Building density ( \# / $\mathrm{km}^2$ )')
# Set axes labels
ax.set_xlabel(r'latitude')
ax.set_ylabel(r'longitude')

ax.set_aspect(1)
# Adjust the location of the axes and labels inside the canvas ('fig') so that
# everything is visible
fig.tight_layout(pad=1)

# Save as SVG for showing in browser
plt.savefig("histogram_2d.svg", dpi=300)
plt.show()

#!/usr/bin/env python3

import numpy as np
import mayavi.mlab as mlab

# Create example data
X,Y,Z = np.mgrid[-5:5:100j,-5:5:100j,-5:5:100j]
R = np.sqrt(X**2+Y**2+Z**2)
TH = np.arccos(Z/R)
psi210 = 1.0/(4*np.sqrt(2*np.pi))*R*np.exp(-R/2)*np.cos(TH)
density = np.abs(psi210)**2

# Prepare figure
fig = mlab.figure(bgcolor=(34/255.,34/255.,34/255.))

# Plot the slices
scalar_field = mlab.pipeline.scalar_field(density)

mlab.pipeline.image_plane_widget(scalar_field,
                                 plane_orientation='x_axes',
                                 slice_index=50,
                                 colormap='plasma', vmin=0,
                                 vmax=np.max(density))

mlab.pipeline.image_plane_widget(scalar_field,
                                 plane_orientation='y_axes',
                                 slice_index=50,
                                 colormap='plasma', vmin=0,
                                 vmax=np.max(density))

mlab.pipeline.image_plane_widget(scalar_field,
                                 plane_orientation='z_axes',
                                 slice_index=50,
                                 colormap='plasma', vmin=0,
                                 vmax=np.max(density))

mlab.savefig("slices.png", magnification=2)

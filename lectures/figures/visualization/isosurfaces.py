#!/usr/bin/env python3

# Use the style for lecture slides
import matplotlib.style

matplotlib.style.use('../slideshow.mplstyle')
from matplotlib.ticker import MaxNLocator

from mpl_toolkits.mplot3d import Axes3D
from skimage import measure # for isosurfaces

import numpy as np
import matplotlib.pyplot as plt

def get_figure(width = 246, aspect=(np.sqrt(5.0)+1)/2.0):
    """
    Returns a matplotlib figure. Default values produces figure whose
    width is 246 pt, i.e, the PRL column width and aspect ~ golden ratio.
    
    Parameters
    ----------
    width       int/float, optional
                width of the figure in 'pt' units (1 pt ≈ 0.3528 mm)
                Defaults to 246 pt, i.e., the width of a single column in
                Physical Review Letters
    aspect      int/float
                Sets width/height to this value

    Returns
    -------
    fig         matplotlib.pyplot.Figure
    """
    assert isinstance(width, (int, float, np.floating)), \
            "type(width) is not int,float or np.floating [was %s]"%repr(type(width))
    assert isinstance(aspect, (int, float, np.floating)), \
            "type(aspect) is not int,float or np.floating [was %s]"%repr(type(aspect))

    width = width/72. # in inches
    height = width / aspect

    return plt.figure(figsize=(width, height))

fig = get_figure(width = 492)

# Create axis with a 3d projection
ax = fig.add_subplot(111, projection='3d')

# Create example data
X,Y,Z = np.mgrid[-10:10:200j,-10:10:200j,-10:10:200j]
R = np.sqrt(X**2+Y**2+Z**2)
TH = np.arccos(Z/R)
psi210 = 1.0/(4*np.sqrt(2*np.pi))*R*np.exp(-R/2)*np.cos(TH)

# Find isosurfaces corresponding to data value 0.0005
verts, faces, normals, values = measure.marching_cubes(np.abs(psi210)**2, 0.0005,
                                      spacing=(0.1,0.1,0.1))

# Plot the triangulation, coloring is just indices of the vertices
m = ax.plot_trisurf(verts[:,0]-10, verts[:,1]-10, faces, verts[:,2]-10,
                cmap='Spectral', zorder=-11)

ax.set_xlabel(r'$x$ (nm)', labelpad = 10)
ax.set_ylabel(r'$y$ (nm)', labelpad = 10)
ax.set_zlabel(r'$z$ (nm)', labelpad = 10)

ax.set_aspect(1)

ax.set_rasterization_zorder(0)
plt.savefig("isosurfaces.png", dpi=300)

#!/usr/bin/env python3

# Use the style for lecture slides
import matplotlib.style

matplotlib.style.use('../slideshow.mplstyle')
from matplotlib.ticker import MaxNLocator


from mpl_toolkits.mplot3d import Axes3D

import numpy as np
import matplotlib.pyplot as plt

def get_figure(width = 246, aspect=(np.sqrt(5.0)+1)/2.0):
    """
    Returns a matplotlib figure. Default values produces figure whose
    width is 246 pt, i.e, the PRL column width and aspect ~ golden ratio.
    
    Parameters
    ----------
    width       int/float, optional
                width of the figure in 'pt' units (1 pt ≈ 0.3528 mm)
                Defaults to 246 pt, i.e., the width of a single column in
                Physical Review Letters
    aspect      int/float
                Sets width/height to this value

    Returns
    -------
    fig         matplotlib.pyplot.Figure
    """
    assert isinstance(width, (int, float, np.floating)), \
            "type(width) is not int,float or np.floating [was %s]"%repr(type(width))
    assert isinstance(aspect, (int, float, np.floating)), \
            "type(aspect) is not int,float or np.floating [was %s]"%repr(type(aspect))

    width = width/72. # in inches
    height = width / aspect

    return plt.figure(figsize=(width, height))

fig = get_figure(width = 492)
ax = fig.add_subplot(111, projection='3d')

X, Y = np.mgrid[-1:1:500j,-1:1:500j]
Z = X**2+np.cos(Y**2)

ax.plot_surface(X,Y,Z, cmap='viridis')

ax.set_xlabel(r'$x$ (nm)', labelpad = 10)
ax.set_ylabel(r'$y$ (nm)', labelpad = 10)
ax.set_zlabel(r'$z$ (nm)', labelpad = 10)

ax.zaxis.set_major_locator(MaxNLocator(4))


plt.savefig("surf.svg")
#plt.show()

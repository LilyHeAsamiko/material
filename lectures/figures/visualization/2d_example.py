#!/usr/bin/env python3

import matplotlib.style
matplotlib.style.use('../slideshow.mplstyle')

import numpy as np
import matplotlib.pyplot as plt

def set_gr(ax):
    """ Sets figure aspect to golden ratio when axes have linear scale """
    x = ax.get_xlim()
    y = ax.get_ylim()
    gr = (np.sqrt(5.0) + 1) / 2
    ax.set_aspect((x[1] - x[0]) / (y[1] - y[0]) / gr)


gr = (np.sqrt(5) + 1) / 2
width = 0.013 * 246 * 2
height = width / gr
fig = plt.figure(figsize=(width, height))

fig.subplots_adjust(bottom=0.18)
ax = fig.add_subplot(111)

x = np.linspace(0, 10, 2000)
y = np.sinh(x)*np.exp(-(x/10)**2)/1000
ax.plot(x,y)
ax.set_xlabel(r'time (minutes)')
ax.set_ylabel(r'distance to home (km)')
set_gr(ax)
plt.savefig("2d_example.svg")
